import React from 'react';
import PropTypes from 'prop-types';

const ResearchItem = ({item}) => {
  return (
    <li className={`bcs-research-item`}>
      <div className={`bcs-research-item-info`}>
        <h3 className={`bcs-reseaerch-item-title`}>
          {item.title}
        </h3>
        <details>
          <summary className={`bcs-reseaerch-item-abstract`}>
            {item.abstract}
          </summary>
        </details>
        <nav className={`bcs-research-item-teamlist`}>
          {
            item.team.map((i) => {
              return (
                <div key={Math.random()} className={`bcs-research-item-teamlist-item`}>
                  <a href={i.link}>
                    <span>
                      {i.name}
                    </span>
                  </a>
                </div>
              );
            })
          }
        </nav>
        <nav className={`bcs-research-item-linklist`}>
          {
            item.links.map((link) => {
              return (
                <div key={Math.random()} className={`bcs-research-item-linklist-item`}>
                  <a href={link.url}>
                    <span>
                      {link.name}
                    </span>
                  </a>
                </div>
              );
            })
          }
        </nav>
      </div>

      <div className={`bcs-research-item-divider`}>
        <hr/>
      </div>

      <div className={`bcs-research-item-img`}>
        <img src={`assets/img/png/logo.png`}/>
      </div>
    </li>
  );
};

ResearchItem.displayName = `ResearchItem`;
ResearchItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default ResearchItem;
