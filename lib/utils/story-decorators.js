import React from 'react';

const CenterDecorator = (story) => {
  return (
    <div className={`center-decorator`}>
      {story()}
    </div>
  );
};

export {
  CenterDecorator
};
