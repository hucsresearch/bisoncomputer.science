/* eslint arrow-body-style: 0 */
import {storiesOf} from '@kadira/storybook';
import React from 'react';
import testdouble from 'testdouble';

import {CenterDecorator} from '../utils/story-decorators';
import ResearchItem from '../components/research-item.js';

storiesOf(`Research`, module)
  .addDecorator(CenterDecorator)
  .add(`Render single item`, () => {
    const fakeItem = testdouble.object({
      title: `Example Project`,
      abstract: `An example project`,
      team: [
        {
          name: `Barry Harris`,
          link: `https://bitbucket.org/brwnrclse`
        }
      ],
      links: [
        {
          url: `https://bitbucket.org/hucsresearch`,
          name: `bitbucket`
        }
      ]
    });

    return <ResearchItem item={fakeItem}/>;
  });
