const bsConfig = {
  files: [`${__dirname}/public/css/*.css`],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`${__dirname}/public/*.html`]
    }
  }],
  server: `public`
};

require(`browser-sync`).init(bsConfig);
