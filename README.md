# bison computer science - home page

---

Site [here](http://bisoncomputerscience.netlify.com)

The student portal for Computer Science at Howard University.

## Design
The site was designed by @brwnrclse using [Figma](https://figma.com).

## Running site (requires npm)
A development version of the site is available from this repo by cloning.

```

git clone https://bitbucket.org/hucsresearch/bisoncomputer.science

# or with ssh

git clone git@bitbucket.org:hucsresearch/bisoncomputer.science

```

The site is then available after installing necessary dependencies and running the bundlded server.

### Install dependencies

```

npm install

# Or using yarm

yarn

```

### Run server

```

npm start

```

## Rapid development

For rapid development (aka "hot reloading"), run the following command after installing dependencies.

```

npm run watch

```